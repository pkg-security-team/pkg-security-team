# This file is sourced by bin/auto-update

ci_file=debian/salsa-ci.yml

create_if_missing $ci_file <<END
---
include:
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/pipeline-jobs.yml
END
record_change "Add Salsa's CI configuration file" $ci_file
