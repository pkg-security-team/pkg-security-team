# This file is sourced by bin/auto-update

SOURCE=$(awk '/^Source:/ {print $2}' debian/control)
CURDIR_NAME=$(basename $PWD)

if [ "$CURDIR_NAME" != "$SOURCE" ]; then
    echo "WARNING: $PWD is not named after the source package ($SOURCE)"
fi

# Maintainer
sed -i -e "s|^Maintainer: .*|Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>|i" debian/control
record_change "Update Maintainer field" debian/control

# Vcs-*
sed -i \
    -e "s|^Vcs-Git: .*|Vcs-Git: https://salsa.debian.org/pkg-security-team/$SOURCE.git|i" \
    -e "s|^Vcs-Browser: .*|Vcs-Browser: https://salsa.debian.org/pkg-security-team/$SOURCE|i" \
    debian/control
record_change "Update Vcs-* fields" debian/control
