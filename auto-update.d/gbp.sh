# This file is sourced by bin/auto-update

gbp_conf=debian/gbp.conf

create_if_missing $gbp_conf <<END
[DEFAULT]
debian-branch = debian/master
pristine-tar = True

[buildpackage]
sign-tags = True

[import-orig]
filter-pristine-tar = True

[pq]
patch-numbers = False

[dch]
multimaint-merge = True
END
record_change "Configure git-buildpackage for Debian" $gbp_conf
